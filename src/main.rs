#[macro_use] extern crate lazy_static;
#[macro_use] extern crate anyhow;

#[macro_use] mod macros;
mod util;
mod core;
mod sound;
mod event_emitters;
mod metronome;
mod interface;
mod cli_config;
mod commands;

use std::sync::{Mutex, Arc};

use getch::Getch;

use crate::core::Engine;
use interface::{Displayer, Keyboard};
use commands::CommandError;
use cli_config::parse_cli;
use crate::util::getchar;

fn main() -> Result<(), anyhow::Error> {
    let cli_config = parse_cli();

    let mut engine = Engine::new(&cli_config)?;
    let keyboard = Arc::new(Mutex::new(Keyboard::new()));
    let displayer = Arc::new(Mutex::new(
        Displayer::new(keyboard.clone())
    ));
    engine.set_displayer(displayer.clone());

    let getch = Getch::new();

    loop {
        lock!(displayer, displayer, {
            lock!(engine.player, player, {
                displayer.update(&player);
            });
        });

        let new_char = getchar(&getch);
        let command = lock!(keyboard, keyboard, {
            keyboard.get_command_from_new_char(new_char)
        });

        if let Some(command) = command {
            match command.exec(&engine.player, Some(&keyboard)) {
                Ok(()) => (),
                Err(CommandError::Quit) => break,
            }
        }
    }

    lock!(displayer, displayer, {
        displayer.erase(true);
    });
    engine.stop();

    Ok(())
}
