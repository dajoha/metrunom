use std::fmt::Display;
use std::str::FromStr;

use clap::{Arg, App};
use cpal::BufferSize;
use indoc::indoc;

use crate::core::MAX_TEMPO;

const VERSION: &str = "0.0.9";

const DEFAULT_VOLUME: f32 = 100.0;
const DEFAULT_TEMPO: f32 = 90.0;
const DEFAULT_FREQUENCY: f32 = 440.0;

// Didn't find a better solution to show the default values in the command line help:
lazy_static! {
    static ref DEFAULT_TEMPO_STR: String = format!("{}", DEFAULT_TEMPO);
    static ref DEFAULT_FREQUENCY_STR: String = format!("{}", DEFAULT_FREQUENCY);
    static ref DEFAULT_VOLUME_STR: String = format!("{}", DEFAULT_VOLUME);
}

#[derive(Debug)]
pub struct CliConfig {
    pub tempo: f32,
    pub volume: f32,
    pub default_frequency: f32,
    pub buffer_size: BufferSize,
    pub jack_audio: bool,
    pub beats_per_bar: i16,
}

impl CliConfig {
    pub fn buffer_size(&self) -> BufferSize {
        self.buffer_size.clone()
    }
}

pub fn parse_cli() -> CliConfig {
    let matches = get_app().get_matches();

    let tempo = match matches.value_of("tempo") {
        Some(s) => s.parse::<f32>().unwrap(),
        None => DEFAULT_TEMPO,
    };

    let volume = match matches.value_of("volume") {
        Some(s) => s.parse::<f32>().unwrap() / 100.0,
        None => 1.0,
    };

    let default_frequency = match matches.value_of("frequency") {
        Some(s) => s.parse::<f32>().unwrap(),
        None => DEFAULT_FREQUENCY,
    };

    let buffer_size = match matches.value_of("buffer_size") {
        Some(s) => BufferSize::Fixed(s.parse::<u32>().unwrap()),
        None => BufferSize::Default,
    };

    let beats_per_bar = match matches.value_of("beats_per_bar") {
        Some(s) => s.parse::<i16>().unwrap(),
        None => 0,
    };

    CliConfig {
        tempo,
        volume,
        default_frequency,
        buffer_size,
        beats_per_bar,
        jack_audio: matches.is_present("jack"),
    }
}

fn get_app() -> App<'static, 'static> {
    App::new("metrunom")
        .version(VERSION)
        .author("Johann DAVID <johann.david.dev@protonmail.com>")
        .about("A command-line interactive metronom.")
        .after_help(indoc!("
            KEY BINDINGS:
             +          Increase the tempo by 1 bpm
             -          Decrease the tempo by 1 bpm
             [NUM]<CR>  Set the tempo to the given number
             [NUM]*     Set the number of beats per bar to the given number
             <Space>    Pause the metronom
             q          Quit
        "))
        .arg(Arg::with_name("tempo")
            .short("t")
            .long("tempo")
            .value_name("BPM")
            .validator(bounded(0.000001_f32, MAX_TEMPO))
            .takes_value(true)
            .default_value(&DEFAULT_TEMPO_STR)
            .help("Sets the initial tempo"))
        .arg(Arg::with_name("volume")
            .short("v")
            .long("volume")
            .value_name("PERCENT")
            .validator(bounded(0_f32, 100_f32))
            .takes_value(true)
            .default_value(&DEFAULT_VOLUME_STR)
            .help("Sets the volume (accepted values: 0 to 100)"))
        .arg(Arg::with_name("frequency")
            .short("f")
            .long("frequency")
            .value_name("FREQ")
            .validator(bounded(20_f32, 20_000_f32))
            .takes_value(true)
            .default_value(&DEFAULT_FREQUENCY_STR)
            .help("Sets the default click wave frequency"))
        .arg(Arg::with_name("beats_per_bar")
            .short("b")
            .long("beats-per-bar")
            .value_name("BEATS")
            .validator(is_parsable::<i16>)
            .takes_value(true)
            .default_value("0")
            .help("Sets the number of beats per bar"))
        .arg(Arg::with_name("buffer_size")
            .short("B")
            .long("buffer-size")
            .value_name("SIZE")
            .validator(bounded(u32::MIN, u32::MAX))
            .takes_value(true)
            .help("Sets the audio frame buffer size"))
        .arg(Arg::with_name("jack")
            .short("j")
            .long("jack")
            .help("Use JACK audio"))
}

fn bounded<T>(min: T, max: T) -> impl Fn(String) -> Result<(), String>
where
    T: PartialOrd + Display + FromStr,
{
    move |v: String| {
        if let Ok(v) = v.parse::<T>() {
            if v >= min && v <= max {
                return Ok(());
            }
        }

        Err(format!("{}: must be a number, bounded between {} and {}", v, min, max))
    }
}

fn is_parsable<T>(v: String) -> Result<(), String>
where
    T: FromStr,
{
    if v.parse::<T>().is_ok() {
        Ok(())
    } else {
        Err(format!("{}: invalid value", v))
    }
}
