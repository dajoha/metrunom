use std::sync::{Arc, Mutex};

use crate::core::Player;
use crate::interface::{Keyboard, DEFAULT_TEMPO_STEP};

#[derive(Debug)]
pub enum Command {
    Quit,
    TogglePause,
    ModifyTempo(ModifDetail),
    SetTempo(f32),
    SetBeatsPerBar(i16),
}

#[derive(Debug)]
pub enum CommandError {
    Quit,
}

impl Command {
    pub fn exec(
        &self,
        player: &Arc<Mutex<Player>>,
        keyboard: Option<&Arc<Mutex<Keyboard>>>
    ) -> Result<(), CommandError> {
        macro_rules! lock_player {
            ($player:ident, $block:block) => {{
                let mut $player = player.lock().unwrap();
                $block;
            }};
        }

        match self {
            Command::Quit => {
                return Err(CommandError::Quit);
            },
            Command::TogglePause => lock_player!(player, {
                player.toggle_pause();
            }),
            Command::ModifyTempo(modif) => lock_player!(player, {
                let tempo = player.tempo();

                let new_tempo = if let Some(keyboard) = keyboard {
                    lock!(keyboard, keyboard, {
                        keyboard.apply_tempo_modif(tempo, &modif)
                    })
                } else {
                    modif.apply(tempo, DEFAULT_TEMPO_STEP)
                };

                player.set_tempo(new_tempo);
            }),
            Command::SetTempo(n) => lock_player!(player, {
                player.set_tempo(*n);
                player.restart();
            }),
            Command::SetBeatsPerBar(n) => lock_player!(player, {
                player.set_beats_per_bar(*n);
                player.restart();
            }),
        }
        Ok(())
    }

    pub fn new_modify_tempo(direction: ModifDirection, value: Option<f32>) -> Self {
        Command::ModifyTempo(ModifDetail {
            direction,
            value,
        })
    }
}

#[derive(Debug, Clone, Copy)]
pub enum ModifDirection {
    Increase,
    Decrease,
}

impl Default for ModifDirection {
    fn default() -> Self {
        Self::Increase
    }
}

#[derive(Debug, Default)]
pub struct ModifDetail {
    pub direction: ModifDirection,
    pub value: Option<f32>,
}

impl ModifDetail {
    pub fn apply(&self, n: f32, default_value: f32) -> f32 {
        let value = self.value.unwrap_or(default_value);

        match self.direction {
            ModifDirection::Increase => n + value,
            ModifDirection::Decrease => n - value,
        }
    }
}
