use std::str::FromStr;

use nom::IResult;
use nom::character::streaming::{digit0, digit1, char};
use nom::combinator::{map_res, recognize, opt};
use nom::sequence::{tuple, pair};

pub fn float<T>(input: &str) -> IResult<&str, T>
where
    T: FromStr
{
    map_res(
        recognize(tuple((
            digit1,
            opt(pair(
                char('.'),
                digit0,
            ))
        ))),
        |d: &str| d.parse::<T>()
    )
    (input)
}

pub fn integer<T>(input: &str) -> IResult<&str, T>
where
    T: FromStr
{
    map_res(
        digit1,
        |d: &str| d.parse::<T>()
    )
    (input)
}
