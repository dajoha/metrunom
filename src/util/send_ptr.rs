pub struct SendPtr<T>(pub *mut T);

unsafe impl<T> Send for SendPtr<T> {}
