use cpal::traits::{DeviceTrait, HostTrait, StreamTrait};
use cpal::{Device, StreamConfig};
use std::sync::{Arc, Mutex};
use std::time::Duration;
use std::thread;

use super::Player;
use crate::sound::anti_pop;
use crate::cli_config::CliConfig;
use crate::interface::Displayer;

pub struct Engine {
    pub player: Arc<Mutex<Player>>,
    pub stream: <Device as DeviceTrait>::Stream,
}

impl Engine {
    pub fn new(cli_config: &CliConfig) -> Result<Engine, anyhow::Error> {
        let host = if cli_config.jack_audio {
            cpal::available_hosts()
                .into_iter()
                .find(|id| *id == cpal::HostId::Jack)
                .and_then(|id| cpal::host_from_id(id).ok())
                .ok_or_else(|| anyhow!("Unable to initialize JACK audio"))?
        } else {
            cpal::default_host()
        };

        let device = host.default_output_device()
            .ok_or_else(|| anyhow!("Failed to find a default output device"))?;

        let default_config = device.default_output_config()?;

        let mut config = default_config.config();
        config.buffer_size = cli_config.buffer_size();

        let player = Arc::new(Mutex::new(Player::new(
            &config,
            cli_config.tempo,
            cli_config.beats_per_bar,
            cli_config.volume,
            cli_config.default_frequency,
        )));

        let stream = match default_config.sample_format() {
            cpal::SampleFormat::F32 => Self::run::<f32>(&device, &config, &player),
            cpal::SampleFormat::I16 => Self::run::<i16>(&device, &config, &player),
            cpal::SampleFormat::U16 => Self::run::<u16>(&device, &config, &player),
        }?;

        Ok(Engine { player, stream })
    }

    pub fn set_displayer(&mut self, displayer: Arc<Mutex<Displayer>>) {
        lock!(self.player, player, {
            player.set_displayer(displayer);
        });
    }

    fn run<T>(device: &Device, config: &StreamConfig, player: &Arc<Mutex<Player>>)
        -> Result<<Device as DeviceTrait>::Stream, anyhow::Error>
    where
        T: cpal::Sample,
    {
        let channels = config.channels as usize;
        let player = player.clone();

        let stream = device.build_output_stream(
            config,
            move |data: &mut [T], _: &cpal::OutputCallbackInfo| {
                Self::write_data(data, channels, &player)
            },
            |err| eprintln!("An error occurred on stream: {}", err),
        )?;

        stream.play()?;

        Ok(stream)
    }

    fn write_data<T>(output: &mut [T], channels: usize, player: &Arc<Mutex<Player>>)
    where
        T: cpal::Sample,
    {
        lock!(player, player, {
            for frame in output.chunks_mut(channels) {
                let value: T = cpal::Sample::from::<f32>(&player.next_sample());
                for sample in frame.iter_mut() {
                    *sample = value;
                }
            }
            player.refresh_display();
        })
    }

    pub fn stop(&mut self) {
        lock!(self.player, player, {
            player.stop();
        });
        thread::sleep(Duration::from_secs_f32(anti_pop::FADE_DURATION + 0.1));
    }
}
