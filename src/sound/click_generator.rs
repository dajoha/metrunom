use super::{AntiPopFader, AdsrEnvelope, SoundGenerator};

const ATTACK: f32 = 0.01;
const DECAY: f32 = 0.03;
const SUSTAIN: f32 = 0.1;
const RELEASE: f32 = 0.1;
const PEAK_LEVEL: f32 = 1.0;
const SUSTAIN_LEVEL: f32 = 0.02;

pub struct ClickGenerator {
    frequency: f32,
    start_time: Option<f32>,
    anti_pop_fader: AntiPopFader,
    adsr: AdsrEnvelope,
}

impl ClickGenerator {
    pub fn new(frequency: f32) -> Self {
        Self {
            frequency,
            start_time: None,
            anti_pop_fader: AntiPopFader::new(),
            adsr: AdsrEnvelope::new(
                ATTACK,
                DECAY,
                SUSTAIN,
                RELEASE,
                PEAK_LEVEL,
                SUSTAIN_LEVEL
            ),
        }
    }

    pub fn set_frequency(&mut self, frequency: f32) {
        self.frequency = frequency;
    }

    pub fn play(&mut self, clock: f32) {
        self.start_time = Some(clock);
        self.anti_pop_fader.fade_in(clock);
        self.adsr.start(clock);
    }
}

impl SoundGenerator for ClickGenerator {
    fn get_sample(&mut self, clock: f32) -> f32 {
        match self.start_time {
            Some(start_time) => {
                let amp = self.anti_pop_fader.get_amp(clock) * self.adsr.get_amp(clock);
                if amp == 0.0 {
                    0.0
                } else {
                    let head = clock - start_time;
                    (head * self.frequency * 2.0 * 3.141592).sin() * amp
                }
            },
            None => 0.0,
        }
    }
}
